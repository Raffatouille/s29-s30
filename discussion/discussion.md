a set of components of disgined to simplify the web dev process
    - convention over configuration

opinionated web framework
    - the framework dictated how it should be used by the dev
    - speeds up the startup process of app development
    - enforces best practices for the framework's use case
    - lack of flexibility could be a drawback when app's needs are not aligned with the framework's assumptions.

unopinionated web framework
    - the dev dictates how to use the framework.
    - offers flexibility to crate an application unbound by any use case
    - no "right way" of structuring an application
    - abundance of options may be overwhelming.


ADV
    -simplicity make it easy to learn and use
    -offers ready to use components